//
//  ViewController.swift
//  Hello_world
//
//  Created by Hevin Parmar on 1/28/17.
//  Copyright © 2017 Hevin Parmar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    @IBOutlet weak var hello: UIImageView!
    @IBOutlet weak var background: UIImageView!
    
    @IBOutlet weak var welcome: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func press(_ sender: Any) {
        background.isHidden = false
        hello.isHidden = false
        welcome.isHidden = true
    }

}

