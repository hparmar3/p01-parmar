//
//  ViewController.h
//  Hevin-2048
//
//  Created by Hevin Parmar on 2/2/17.
//  Copyright © 2017 Hevin Parmar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *tiles; //tiles 4*4

@end

